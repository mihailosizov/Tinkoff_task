package ru.yandex.market.pages;

import common.driver.Driver;
import common.utils.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public abstract class NavigationPage {
    @FindAll(@FindBy(xpath = "//ul[@class='topmenu__list']/li[@class='topmenu__item i-bem topmenu__item_js_inited']"))
    @CacheLookup
    private List<WebElement> topNavigationMenus;

    private static final String ELECTRONICS_MENU = "Электроника",
            SPIN_PROGRESS_XPATH = "//div[contains(@class,'spin2_progress_yes')]";

    private static final int SPIN_PROGRESS_START_TIMEOUT_MS = 2500,
            SPIN_PROGRESS_WAIT_TIMEOUT_MS = 15000;


    protected WebDriver driver;

    NavigationPage() {
        driver = Driver.getInstance();
        PageFactory.initElements(driver, this);
    }

    public void waitForProgress() {
        Utils.waitForElementToDisappear(By.xpath(SPIN_PROGRESS_XPATH), SPIN_PROGRESS_START_TIMEOUT_MS,
                SPIN_PROGRESS_WAIT_TIMEOUT_MS);
    }

    public ElectronicsPage openElectonicsPage() {
        openNavigationMenuItem(ELECTRONICS_MENU);
        return ((ElectronicsPage) Utils.pageOpener(ElectronicsPage.class, ElectronicsPage.TITLE));
    }

    protected void openNavigationMenuItem(String itemName) {
        for (WebElement element : topNavigationMenus) {
            if (element.getAttribute("data-department").equals(itemName)) {
                element.click();
                break;
            }
        }
    }
}
