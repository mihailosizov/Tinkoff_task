package ru.yandex.market.pages;

import common.utils.Utils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static ru.yandex.market.pages.CellPhonesAdvancedSearchPage.FoundDevice.*;

public class CellPhonesAdvancedSearchPage extends NavigationPage {
    @FindBy(xpath = "//div[@class='layout__col layout__col_search-filters_visible i-bem']")
    private WebElement searchParametersBlock;

    @FindBy(xpath = "//input[@id='glf-pricefrom-var']")
    private WebElement priceFromInput;

    @FindBy(xpath = "//input[@id='glf-priceto-var']")
    private WebElement priceToInput;

    @FindBy(xpath = "//label[@class='checkbox__label' and text()='В продаже']/../span/input")
    private WebElement inStockCheckBox;

    @FindBy(xpath = "//label[@class='checkbox__label' and text()='Android']/../span/input")
    private WebElement androidOsCheckBox;

    @FindBy(xpath = "//span[@class='title__content' and text()='Тип']")
    private WebElement deviceTypeBlockTrigger;

    @FindBy(xpath = "//span[@class='title__content' and text()='Тип']/../../following-sibling::div/div")
    private WebElement deviceTypeBlock;

    @FindAll(@FindBy(xpath = "//span[@class='title__content' and text()='Тип']/../../following-sibling::div/div/div"))
    private List<WebElement> deviceTypeBlockElements;

    @FindAll(@FindBy(xpath = "//div[contains(@class,'snippet-list snippet-list_type_vertical island')]/div[contains(@class,'snippet-card')]"))
    private List<WebElement> foundDevices;

    public static final String TITLE = "Мобильные телефоны — купить на Яндекс.Маркете",
            DEVICE_TYPE_CHECKBOX_LABEL_XP = ".//label[@class='checkbox__label']",
            DEVICE_TYPE_CHECKBOX_XP = "./../span/input";


    public static class FoundDevice {
        static final String RATING_XPATH = ".//div[contains(@class,'rating hint')]",
                PRICE_XPATH = ".//span[@class='price']", TITLE_XPATH = ".//a[@title]";

        int index;
        double rating;
        String name;
        String priceRange;

        public FoundDevice(int index, double rating, String name, String priceRange) {
            this.index = index;
            this.rating = rating;
            this.name = name;
            this.priceRange = priceRange;
        }

        public int getIndex() {
            return index;
        }

        public double getRating() {
            return rating;
        }

        public String getName() {
            return name;
        }

        public String getPriceRange() {
            return priceRange;
        }
    }

    public enum DeviceType {
        SMARTPHONE("смартфон"),
        TELEPHONE("телефон"),
        TELEPHONE_FOR_KIDS("телефон для детей"),
        TELEPHONE_FOR_ELDERS("телефон для пожилых");

        private String visibleName;

        DeviceType(String visibleName) {
            this.visibleName = visibleName;
        }

        public String getVisibleName() {
            return visibleName;
        }
    }

    public List<FoundDevice> collectFoundDevicesInfo() {
        List<FoundDevice> deviceList = new ArrayList<>();
        for (WebElement element : foundDevices) {
            int index = foundDevices.indexOf(element) + 1;
            WebElement ratingElement = element.findElement(By.xpath(RATING_XPATH));
            double rating = Double.valueOf(ratingElement.getText());
            List<WebElement> priceElements = element.findElements(By.xpath(PRICE_XPATH));
            StringBuilder priceRange = new StringBuilder();
            Iterator<WebElement> iterator = priceElements.iterator();
            while (iterator.hasNext()) {
                priceRange.append(iterator.next().getText());
                if (iterator.hasNext())
                    priceRange.append(" - ");
            }
            WebElement titleElement = element.findElement(By.xpath(TITLE_XPATH));
            String title = titleElement.getText();
            deviceList.add(new FoundDevice(index, rating, title, priceRange.toString()));
        }
        return deviceList;
    }

    public boolean setDeviceTypeFromDeviceTypeBlock(DeviceType deviceType, boolean setSelected) {
        for (WebElement blockElement : deviceTypeBlockElements) {
            WebElement label = blockElement.findElement(By.xpath(DEVICE_TYPE_CHECKBOX_LABEL_XP));
            if (label.getText().trim().equals(deviceType.getVisibleName())) {
                WebElement checkBox = label.findElement(By.xpath(DEVICE_TYPE_CHECKBOX_XP));
                if (checkBox.isSelected() ^ setSelected) checkBox.click();
                return checkBox.isSelected() == setSelected;
            }
        }
        return false;
    }

    public WebElement getSearchParametersBlock() {
        return searchParametersBlock;
    }

    public boolean setSearchParameterPriceFrom(String price) {
        priceFromInput.sendKeys(price);
        return Utils.isElementValueEquals(priceFromInput, price);
    }

    public boolean setSearchParameterPriceTo(String price) {
        priceToInput.sendKeys(price);
        return Utils.isElementValueEquals(priceToInput, price);
    }

    public boolean setSearchParameterInStock(boolean value) {
        if (inStockCheckBox.isSelected() ^ value) inStockCheckBox.click();
        return inStockCheckBox.isSelected() == value;
    }

    public boolean setSearchParameterAndroidOs(boolean value) {
        if (androidOsCheckBox.isSelected() ^ value) androidOsCheckBox.click();
        return androidOsCheckBox.isSelected() == value;
    }

    public boolean setDeviceTypeBlockVisible(boolean visible) {
        if (Utils.isElementPresentAndDiplayed(deviceTypeBlock) ^ visible) deviceTypeBlockTrigger.click();
        return Utils.isElementPresentAndDiplayed(deviceTypeBlock);
    }
}
