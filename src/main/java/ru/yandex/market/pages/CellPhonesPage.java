package ru.yandex.market.pages;

import common.utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class CellPhonesPage extends NavigationPage {
    @FindAll(@FindBy(xpath = "//div[@class='top-3-models']//a[text()='Популярные']/../../ul[@class='top-3-models__list']/li[@class='top-3-models__model']"))
    @CacheLookup
    private List<WebElement> popularModelList;

    @FindAll(@FindBy(xpath = "//div[@class='top-3-models']//a[text()='Новинки']/../../ul[@class='top-3-models__list']/li[@class='top-3-models__model']"))
    @CacheLookup
    private List<WebElement> newModelList;

    @FindBy(xpath = "//a[contains(.,'Расширенный поиск')]")
    @CacheLookup
    private WebElement advancedSearchLink;

    public static final String TITLE = "Мобильные телефоны - выбирайте и покупайте на Яндекс.Маркете";

    public CellPhonesAdvancedSearchPage openAdvancedSearchLink() {
        advancedSearchLink.click();
        return ((CellPhonesAdvancedSearchPage) Utils.pageOpener(CellPhonesAdvancedSearchPage.class,
                CellPhonesAdvancedSearchPage.TITLE));
    }

    public List<WebElement> getPopularModelList() {
        return popularModelList;
    }

    public List<WebElement> getNewModelList() {
        return newModelList;
    }
}
