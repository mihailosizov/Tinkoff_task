package ru.yandex.market.pages;

import common.utils.Utils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class ElectronicsPage extends NavigationPage {
    @FindAll(@FindBy(xpath = "//div[@class='catalog-menu i-bem catalog-menu_js_inited']/div[@class='catalog-menu__item']//a[@href]"))
    @CacheLookup
    private List<WebElement> leftMenusLinks;

    private static String CELL_PHONES_LINK = "Мобильные телефоны";

    public static final String TITLE = "Электроника — купить на Яндекс.Маркете";

    private void openLeftMenuLink(String linkText) {
        for (WebElement element : leftMenusLinks) {
            if (element.getAttribute("textContent").trim().equals(linkText)) {
                if (element.isDisplayed()) {
                    element.click();
                    break;
                } else {
                    String link = element.getAttribute("href");
                    driver.navigate().to(link);
                    break;
                }
            }
        }
    }

    public CellPhonesPage openCellPhonesPage() {
        openLeftMenuLink(CELL_PHONES_LINK);
        return ((CellPhonesPage) Utils.pageOpener(CellPhonesPage.class, CellPhonesPage.TITLE));
    }
}
