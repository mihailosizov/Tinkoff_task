package ru.yandex.market.pages;

import common.driver.Driver;
import common.utils.Utils;

public class MainPage extends NavigationPage {

    public static final String URL = "http://market.yandex.ru";
    public static final String TITLE = "Яндекс.Маркет";

    public static MainPage open() {
        Driver.getInstance().navigate().to(URL);
        return ((MainPage) Utils.pageOpener(MainPage.class, TITLE));
    }
}
