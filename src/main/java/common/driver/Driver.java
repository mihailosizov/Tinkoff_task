package common.driver;

import common.support.Log;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;

import java.util.concurrent.TimeUnit;

public class Driver {
    private static boolean isInitialized = false;
    private static WebDriverWait wait;
    private static WebDriver instance;
    public static final long STANDARD_IMPL_TIMEOUT_SECONDS = 10;
    private static final Logger LOGGER = Log.getLogger(Driver.class);

    private Driver() {
    }

    public static WebDriver getInstance() {
        return instance;
    }

    public static WebDriverWait getWait() {
        return wait;
    }

    public static void initialize() {
        if (!isInitialized) {
            LOGGER.info("Driver is not initialized. Starting to initialize webdriver");
            System.setProperty("webdriver.chrome.driver", Driver.class.getResource("/chromedriver.exe").getPath());
            instance = new ChromeDriver();
            wait = new WebDriverWait(instance, STANDARD_IMPL_TIMEOUT_SECONDS);
            instance.manage().timeouts().implicitlyWait(STANDARD_IMPL_TIMEOUT_SECONDS, TimeUnit.SECONDS);
            instance.manage().window().maximize();
            LOGGER.info("Driver has been successfully initialized");
            isInitialized = true;
        }
    }

    public static void close() {
        LOGGER.info("Closing webdriver instance...");
        instance.quit();
        LOGGER.info("Webdriver has been closed");
        isInitialized = false;
    }
}
