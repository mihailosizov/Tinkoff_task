package common.utils;

import common.driver.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.util.List;

import static common.driver.Driver.STANDARD_IMPL_TIMEOUT_SECONDS;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;

public class Utils {
    public static boolean titleEquals(String expectedTitle) {
        return Driver.getInstance().getTitle().trim().equals(expectedTitle);
    }

    public static Object pageOpener(Class<?> clazz, String expectedTitle) {
        if (Utils.titleEquals(expectedTitle)) try {
            return clazz.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        else throw new RuntimeException("Unable to open " + clazz.getSimpleName());
    }

    public static boolean isElementPresentAndDiplayed(WebElement element) {
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public static boolean isElementValueEquals(WebElement element, String value) {
        return element.getAttribute("value").equals(value);
    }

    /**
     * Wait for element, for example progress bar, to disappear before <tt>msWaitTimeout</tt> expires.
     * It is possible to adjust special timeout before waiting for element to disappear, because
     * in some cases element (e.g. progress bar) appears with some delay.
     *
     * @param elementLocator             locator of element to wait
     * @param msTimeoutBeforeStartToWait timeout before wait starts
     * @param msWaitTimeout              timeout during which element should disappear from page
     * @throws RuntimeException if <tt>msWaitTimeout</tt> expired
     */
    public static void waitForElementToDisappear(By elementLocator, int msTimeoutBeforeStartToWait, int msWaitTimeout) {
        Driver.getInstance().manage().timeouts().implicitlyWait(500, MILLISECONDS);
        boolean disappeared = false;
        try {
            MILLISECONDS.sleep(msTimeoutBeforeStartToWait);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        try {
            for (int i = 0; i < msWaitTimeout; i++) {
                MILLISECONDS.sleep(1);
                List<WebElement> elementsToNotBeFound = Driver.getInstance().findElements(elementLocator);
                if (elementsToNotBeFound.size() == 0) {
                    disappeared = true;
                    break;
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            Driver.getInstance().manage().timeouts().implicitlyWait(STANDARD_IMPL_TIMEOUT_SECONDS, SECONDS);
        }
        if (!disappeared) throw new RuntimeException("Unable to wait for element to disappear");
    }
}
