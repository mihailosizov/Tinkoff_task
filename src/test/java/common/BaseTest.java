package common;

import common.driver.Driver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class BaseTest {
    @BeforeSuite(alwaysRun = true)
    public void setupBeforeSuite() {
        Driver.initialize();
    }

    @AfterSuite(alwaysRun = true)
    public void setupAfterSuite() {
        Driver.close();
    }
}
