package ru.yandex.market;

import common.BaseTest;
import common.support.Log;
import common.utils.Utils;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import ru.yandex.market.pages.CellPhonesAdvancedSearchPage;
import ru.yandex.market.pages.CellPhonesPage;
import ru.yandex.market.pages.ElectronicsPage;
import ru.yandex.market.pages.MainPage;

import java.util.Collections;
import java.util.List;

import static common.support.Assert.*;
import static org.hamcrest.Matchers.*;

public class CellPhonesCatalogTest extends BaseTest {
    private static final Logger LOGGER = Log.getLogger(CellPhonesCatalogTest.class);

    private MainPage mainPage;
    private ElectronicsPage electronicsPage;
    private CellPhonesPage cellPhonesPage;
    private CellPhonesAdvancedSearchPage advancedSearchPage;

    @Parameters({"price-from", "price-to", "rating-from", "rating-to", "quantity"})
    @Test
    public void cellPhonesCatalogAdvancedSearchTest(@Optional("5125") String priceFrom, @Optional("10123") String priceTo,
                                                    @Optional("3.5") String ratingFrom, @Optional("4.5") String ratingTo,
                                                    @Optional("3") String deviceQuantity) {
        mainPage = MainPage.open();
        electronicsPage = mainPage.openElectonicsPage();
        cellPhonesPage = electronicsPage.openCellPhonesPage();
        List<WebElement> popularModels = cellPhonesPage.getPopularModelList();
        List<WebElement> newModels = cellPhonesPage.getNewModelList();
        assertThat("There are 3 models in popular models list", popularModels.size(), equalTo(3));
        assertThat("There are 3 models in new models list", newModels.size(), equalTo(3));

        advancedSearchPage = cellPhonesPage.openAdvancedSearchLink();
        boolean searchParametersBlockVisible = Utils.isElementPresentAndDiplayed(
                advancedSearchPage.getSearchParametersBlock());
        assertThat("Search parameters block is displayed", searchParametersBlockVisible);

        boolean priceFromSet = advancedSearchPage.setSearchParameterPriceFrom(priceFrom);
        boolean priceToSet = advancedSearchPage.setSearchParameterPriceTo(priceTo);
        assertThat("Price-from has been set", priceFromSet);
        assertThat("Price-to has been set", priceToSet);

        boolean inStockSet = advancedSearchPage.setSearchParameterInStock(true);
        assertThat("In-stock check box has been set to true", inStockSet);

        boolean deviceTypeBlockVisible = advancedSearchPage.setDeviceTypeBlockVisible(true);
        assertThat("Device type block is visible", deviceTypeBlockVisible);

        boolean deviceTypeSelected = advancedSearchPage.setDeviceTypeFromDeviceTypeBlock(
                CellPhonesAdvancedSearchPage.DeviceType.SMARTPHONE, true);
        assertThat("Device type check-box has been selected", deviceTypeSelected);

        boolean androidOsSelected = advancedSearchPage.setSearchParameterAndroidOs(true);
        assertThat("Android OS check-box has been selected", androidOsSelected);

        advancedSearchPage.waitForProgress();

        List<CellPhonesAdvancedSearchPage.FoundDevice> foundDevices = advancedSearchPage.collectFoundDevicesInfo();
        Collections.shuffle(foundDevices);
        int deviceCount = 0;
        StringBuilder devicesMessage = new StringBuilder();
        for (CellPhonesAdvancedSearchPage.FoundDevice device : foundDevices) {
            if (device.getRating() <= Double.valueOf(ratingTo) && device.getRating() >= Double.valueOf(ratingFrom)) {
                devicesMessage.append(device.getIndex())
                        .append(" - ")
                        .append(device.getName())
                        .append(" - ")
                        .append(device.getPriceRange())
                        .append("\n");
                deviceCount++;
            }
            if (deviceCount == Integer.valueOf(deviceQuantity)) break;
        }
        LOGGER.info(deviceQuantity + " random devices from page:\n" + devicesMessage.toString());
    }
}
